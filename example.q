/Load in krb5 support for kdb+. Amend your path appropriately.

\l qKerberos.q

/Example to get credentials (a ticket granting ticket) if you don't have suitable credentials in your /tmp folder.
/First parameter is the tgt server.
/Second parameter is the username of the principle wishing to acquire credentials.
/Third parameter is the password for the principle.
/Returns credentials that are accesible in memory.

acquireCreds["krbtgt/AHMAD.IO@";"neueda";"foobar"]

/Once you have credentials either through kinit or via the above method run the following function.
/First parameter is the service you want to connect to.
/Second parameter is the host. This can be an ip, a FQDN, or just localhost.
/Third parameter is the port the process you are trying to connect is listening on.
/Returns handle h upon a successful connection.

kerberosClient2ServerAuth["host@krb5";"krb5";5001]
