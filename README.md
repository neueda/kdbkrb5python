# qKerberos

qKerberos is designed as a proof of concept to show that Kerberos can be integrated into a q system. It allows for Kerberos credentials to be acquired, mutual authorisation to be performed between two q services as well as some support for Kerberos admin, namely changing a principles password. 

## Pre-Requisites

- Linux host
- python3.6-8
- krb5
- kdb+ v3.5 64bit or above

## Setting up your Environment

To use this code it requires that the python modules kerberos and gssapi as well as kx’s embedpy are installed on the system. The python modules can be readily installed using the following commands:

-   pip3 install gssapi

- 	pip3 install kerberos

If you get a 'krb5-config: not found' error install the libkrb5 development package:

-   sudo apt install libkrb5-dev


The embedpy repository can be found at the following link:

[embedpy](https://github.com/kxsystems/embedpy)

To install embedpy perform the following steps:
-  	git clone https://github.com/KxSystems/embedPy.git
-   make p.so
-   put p.q and p.k in $QHOME and p.so in $QHOME/l64

One other thing to be aware of is after installing embedpy and trying to load it into a q session, q can signal a ‘libpython error. This can be caused by embedpy not finding the libpython shared object in the /usr/lib directory. Find where this shared object is and copy it into /usr/lib. 

If you need help finding the libpython shared object perform the following steps:

- pip3 install find_libpython
- python3 -m find_libpython

This will output it's location.

This same error can be caused by your python not being enabled to allow shared libraries. Refer, to this kx [link](https://code.kx.com/q/ml/embedpy/faq/) on embedpy to deal with this cause.

Finally, it assumes you have Kerberos 5, ironically the third version of Kerberos, installed throughout your system.

## Using qKerberos

To clone the repo: 

- git clone https://gitlab.com/neueda/kdbkrb5python.git 

Then move the qKerberos.q file int your q directory. 

To work the script must be loaded into both the q client and the q server as follows: 

- \l qKerberos.q 

and you should start you server listening on a port:

- \p 5001

To perform Kerberos authorisation between the client and the server run the following q function on the client: 

- kerberosClient2ServerAuth["host@krb5";"krb5";5001] 

where the first parameter is the service principle in the form “type@FQDN”. You can of course replace the servers FQDN with its IP etc. The second is the host address for the q server, again given here as its FQDN. Finally, the third parameter is the port number the server is listening on.  

To use this function the client needs to have valid credentials. If credentials have been obtained external from the q service through running kerberos’ kinit command for example this function will grab them from the ~/tmp directory. However, if no credentials have been obtained yet they can be obtained from within the q client as described next. 

To acquire credentials you can run the following q function on the client: 

- acquireCreds["krbtgt/AHMAD.IO@";"neueda";"foobar"] 

where the first parameter is the name for the authentication server, the second parameter is the principle's name and the third parameter is the principle's password. 

To change a principles password execute the following q function: 

- changePassword[“neueda”;”foobar”;”fizzbuzz”] 

where the first parameter is the principle's name, the second parameter is the old password and the third parameter is the new password. 

