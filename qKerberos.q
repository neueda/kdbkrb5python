\l p.q

\c 2000 2000

gssapi:.p.import`gssapi;

kerb:.p.import`kerberos;


/Client side functions:

changePassword:{[user;oldPass;newPass] 
	kerb[`:changePassword;user;oldPass;newPass]};

acquireCreds:{[tgtServer;username;password] 
	.[{[tgtServer;username;password] 
		serverName:gssapi[`:Name;tgtServer];
		nameType:gssapi[`:NameType;`:user];
		user:gssapi[`:Name;username;nameType];
		creds:gssapi[`:raw;`:acquire_cred_with_password;user;"x"$password;0];
		show "Success"};
	(tgtServer;username;password);
	{"Error when trying to acquire credentials: - ",x}]};

clientInit:{[service] 
	kerb[`:authGSSClientInit;service]};

clientStep:{[context;challenge] 
	if[-11h~type context;context:value context];
	kerb[`:authGSSClientStep;context;challenge]};

clientResponse:{[context] 
	if[-11h~type context;context:value context];
	kerb[`:authGSSClientResponse;context]};

kerberosClient2ServerAuth:{[service;host;port] 
	.[{[service;host;port] 
		clientContext::(clientInit[service]`)[1];
		clientStep[clientContext;""];
		h::hopen ":",":" sv (host;string[port];service;clientResponse[clientContext]`)};
	(service;host;port);
	{"Error when trying to authenticate with the server: - ",x}]};
	

/Server side functions:

serverInit:{[service] 
	kerb[`:authGSSServerInit;service]};

serverStep:{[context;challenge] 
	kerb[`:authGSSServerStep;context;challenge]};

serverResponse:{[context] 
	kerb[`:authGSSServerResponse;context]};

.z.pw:{[u;p] 
	.[{[u;p] 
		serverContext::(serverInit[u]`)[1];
		"b"$serverStep[serverContext;p]`};
	(u;p);
	{"Error: - ",x}]};
	
.z.po:{[] 
	(neg .z.w)(`clientStep;`clientContext;serverResponse[serverContext]`);
	(neg .z.w)(`clientResponse;`clientContext)};

